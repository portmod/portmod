Installation on Linux
=====================

Linux installs additionally require bubblewrap to run. Please install
`bubblewrap <https://github.com/projectatomic/bubblewrap>`__ (also known
as ``bwrap``) via your package manager. See :ref:`sandbox` for
details.
